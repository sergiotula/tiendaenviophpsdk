## Classes

#### TiendaEnvioService

To create an instance of this class the constructor requires:
> $api_key : string <br/>
  $api_token : string <br/>

To create an instance in manual mode the constructor requires:
> $api_key : string <br/>
  $api_token : string <br/>
  $manual : set this to 'true' <br/>
  $packages_amount : int <br/>

Methods:

* addProduct()
> $identifier <br/>
  $weight <br/>
  $weight_unit <br/>
  $width <br/>
  $height <br/>
  $depth <br/>
  $price <br/>
  $name <br/>
  $sku <br/>
  $code <br/>
  $description <br/>
  $quantity <br/>

* requestRates(): Returns a **TiendaEnvioRates** array
> $source_address_id <br/>
  $dest_zip <br/>
 
* requestCreateShipment()
> $origin_id <br/>
  $origin_code <br/>
  $delivery_type <br/>
  $pick_up_point_id <br/>
  $address_id <br/>
  $service_code <br/>
  $comments <br/>
  $customer_tracking_id <br/>
  $destination : **TiendaEnvioDestination**<br/>

* validateKeys(): **bool**
> 

* getTracking()
>  TODO
* getTrackingStatus()
>  TODO


#### TiendaEnvioRate
Represents a single shipping method<br/>
* Properties
> $serviceId <br/>
  $price <br/>
  $priceTax <br/>
  $priceInsurance <br/>
  $priceTotal <br/>
  $name <br/>
  $code <br/>
  $currency <br/>
  $days <br/>
  $deliveryType <br/>
  $Pickups : **TiendaEnvioPickup[]** <br/>

#### TiendaEnvioPickup
Represents a Pickup Point<br/>
* Properties
> $id <br/>
  $nom <br/>
  $streetName <br/>
  $streetNumber <br/>
  $buildingFloor <br/>
  $buildingRoom <br/>
  $zip <br/>
  $town <br/>
  $departmentName <br/>
  $countryName <br/>
  $phone <br/>
  $email <br/>
  $latitud <br/>
  $longitud <br/>
  $geolocalizacion <br/>
  $opendate_info : **TiendaEnvioOpenDateInfo** <br/>

#### TiendaEnvioOpenDateInfo
Represents a Pickup Point available times<br/>
* Properties
> $dia <br/>
  $inicio <br/>
  $cierre <br/>

#### TiendaEnvioDestination
Represents a physical address<br/>
* Properties
> $destName <br/>
  $destCode  <br/>
  $destCodeType  <br/>
  $destEmail  <br/>
  $destStreetName  <br/>
  $destStreetNumber  <br/>
  $destBuildingFloor  <br/>
  $destBuildingRoom  <br/>
  $destZip  <br/>
  $destTown  <br/>
  $destDepartmentId  <br/>
  $destCountryId  <br/>
  $destPhone  <br/>
  $hourFrom  <br/>

#### TiendaEnvioAddress
* Properties
> $id <br/>
  $name <br/>
  $streetName <br/>
  $streetNumber <br/>
  $buildingFloor <br/>
  $buildingRoom <br/>
  $fullAddress <br/>
  $zip <br/>
  $town <br/>
  $department <br/>
  $country <br/>
  $phone <br/>
  $email <br/>
  $disabled <br/>
  $lat <br/>
  $lng <br/>
  $receive <br/>
* Methods
> getAddress()
  getAddressWithTown()
  getAddressWithZip()


#### TiendaEnvioConst
This class provides information on the current limits for a shipment
* Consts
> WEIGHT_LIMIT_PER_PACKAGE  <br/>
  PRODUCT_PER_PACKAGE_LIMIT  <br/>
  PACKAGE_COUNT_LIMIT  <br/>
  DIMENSION_MIN_LIMIT_PER_PRODUCT  <br/>
  DIMENSION_MAX_LIMIT_PER_PRODUCT  <br/>
  PACKAGE_VOLUME_LIMIT  <br/>
 