# Midla - TiendaEnvio PHP SDK

This package is meant to facilitate integration with TiendaEnvio

---
## Installation instructions
1) Install through composer:
> composer require midla/tiendaenvio-php-sdk

#### Keys
**$origin_id** and **$origin_code** are specific for each platform that is being integrated. <br/>
To obtain them contact us at soporte@tiendaenvio.com.ar <br/>
If possible keep them secret but it is not required.

Each user has their own **$api_key** and **$api_token** pair. <br/>
They can be found on: "Configuración"->"Integraciones". <br/>
Each user must configure their own keys in the external platform and your integration must use them to make requests.

## Validate Keys
1) Create a new instance of **TiendaEnvioService**, while providing 
*$api_key* and *$api_token*
   > $api_key = 'USER_API_KEY';<br/>
     $api_token = 'USER_API_TOKEN';<br/>
     $tiendaenvio_service = new TiendaEnvioService($api_key, $api_token);
2) Validate keys with *validateKeys()*;
   > $isValid = $tiendaenvio_service->validateKeys();<br/>
     if( $isValid ){<br/>
     &nbsp;&nbsp;&nbsp;&nbsp;    echo 'The keys are valid';<br/>
     }

## Request rates
Get a list of available shipping methods
1) Create a new instance of **TiendaEnvioService**, while providing 
   > $api_key and $api_token
     $api_key = 'USER_API_KEY';<br/>
     $api_token = 'USER_API_TOKEN';<br/>
     $tiendaenvio_service = new TiendaEnvioService($api_key,$api_token);
2) Add as many products as needed through the method *addProduct()*
   > $tiendaenvio_service->addProduct($identifier, $weight, $weight_unit, $width, $height, $depth, $price, $name, $sku, $code, $description, $quantity); //$quantity is optional and defaults to 1
3) Choose an address id from your available addresses
   > $addresses = $tiendaenvio_service->getAddresses();<br/>
     $source_address_id = $addresses[0]->id; //Change 0 to your prefered address
4) Send a request by calling *requestCreateShipment()*:
   > $rates = $tiendaenvio_service->request_rates($source_address_id, $dest_zip);


#### Merchant Address
The lines
>  $tiendaenvio_service->getAddresses(); <br/>
   $source_address_id = $addresses[0]->id;
   
get an array with the merchant configured addresses and forces the use of the first one. <br/>
Your integration should allow for the merchant to configure which address should be used. <br/>

---

## Create Shipment
Create a new Shipment
1) Create a new instance of **TiendaEnvioService**, while providing *$api_key* and *$api_token*
   > $api_key = 'USER_API_KEY';<br/>
     $api_token = 'USER_API_TOKEN';<br/>
     $tiendaenvio_service = new TiendaEnvioService($api_key, $api_token);
2) Add as many products as needed through the method *addProduct()*
   > $tiendaenvio_service->addProduct($identifier, $weight, $weight_unit, $width, $height, $depth, $price, $name, $sku, $code, $description, $quantity);  //$quantity is optional and defaults to 1
3) Choose an address id from the merchant available addresses
   > $addresses = $tiendaenvio_service->getAddresses();<br/>
     $source_address_id = $addresses[0]->id; //Change 0 to your prefered address
4) Send a rates request by calling the method *requestRates()*:
   > $rates = $tiendaenvio_service->requestRates($source_address_id, $dest_zip);
5) Choose a shipping method:
   > $method = $rates[0]; //Change 0 to your prefered method
6) Create a **TiendaEnvioDestination**
   > $destination = new TiendaEnvioDestination(
        $destName, 
        $destCode, 
        $destCodeType, 
        $destEmail, 
        $destStreetName, 
        $destStreetNumber, 
        $destBuildingFloor, 
        $destBuildingRoom, 
        $destZip, 
        $destTown, 
        $destDepartmentId, 
        $destCountryId, 
        $destPhone, 
        $hourFrom);<br/>
7) Send a create shipment request by calling the method *requestCreateShipment()*
   > $origen_id = 'PLATFORM_ORIGIN_ID';<br/>
     $origen_code = 'PLATFORM_ORIGIN_CODE';<br/>
     $delivery_type = $method->deliveryType;<br/>
     $pick_up_point_id = null;<br/>
     $service_code = $method->code;<br/>
     $comments = null;<br/>
     $customer_tracking_id = null;<br/>
   > $tiendaenvio_service->requestCreateShipment(
           $origin_id,
           $origin_code,
           $delivery_type,
           $pick_up_point_id,
           $source_address_id,
           $service_code,
           $comments,
           $customer_tracking_id,
           $destination);<br/>


---

## Product managing

#### Restrictions
Adding a new product will set it into a package.<br/>

**Product limitations:**<br/>
A ***LimitException*** will be thrown if the following requirements are not met:<br/>
* Each dimension(width,height,length) can not be less than TiendaEnvioConst::DIMENSION_MIN_LIMIT_PER_PRODUCT<br/>
* Each dimension(width,height,length) can not be greater than TiendaEnvioConst::DIMENSION_MAX_LIMIT_PER_PRODUCT<br/>

**Package limitations:**<br/>
When adding a product a new package will be created if the following limits are reached:<br/>
* Package maximum weight = TiendaEnvioConst::WEIGHT_LIMIT_PER_PACKAGE<br/>
* Package maximum amount of different types of products = PRODUCT_PER_PACKAGE_LIMIT<br/>

When adding products in manual mode, a ***LimitException*** will be thrown instead of creating a new package

**Shipment limitations:**<br/>
A ***LimitException*** will be thrown in the following cases:<br/>
* Maximum amount of packages in a single shipment = TiendaEnvioConst::PACKAGE_COUNT_LIMIT <br/>
 Note that adding products through *addProduct()* may cause the creation of a new package
* This limit also applies when creating a new instance through TiendaEnvioService constructor and setting its fourth argument
---

## Manual product sorting
As an alternative, if you need to place products on specific packages instead of letting the sdk do it for you, follow these steps:
1) Create a new instance of **TiendaEnvioService**, while providing *$api_key* and *$api_token*
   > $api_key = 'USER_API_KEY';<br/>
     $api_token = 'USER_API_TOKEN';<br/>
     $amount_of_packages = 3; //Change this to your desired amount</br>
     $tiendaenvio_service = new TiendaEnvioService($api_key, $api_token, true, $amount_of_packages);

2) Create a new **TiendaEnvioProduct**
   > $product = new TiendaEnvioProduct($identifier, $weight, $weight_unit, $width, $height, $depth, $price, $name, $sku, $code, $description);<br/>

3) Add your product through the method *manualAddProduct()*
   > $package_index = 2;//Change this to your desired package. 0-based array
     $tiendaenvio_service->manualAddProduct($package_index, $product); <br/>
* Optionally, if you are adding several of the same product
   > $products_quantity = 2;//Change this to your desired amount<br/>
     $tiendaenvio_service->manualAddProduct($package_index, $product, $products_quantity); <br/>

