## Helper methods

#### getCountries()
Returns an array of TiendaEnvioCountry

#### findCountry($name)
$name is part of the country name<br/>
Returns an array of TiendaEnvioCountry

#### getProvinces($country_id)
Returns an array of TiendaEnvioProvince
