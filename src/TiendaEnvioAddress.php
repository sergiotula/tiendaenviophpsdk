<?php


namespace Midla\TiendaenvioPhpSdk;

/**
 * Class TiendaEnvioAddress
 *
 * @property int $id
 * @property string $name
 * @property string $streetName
 * @property string $streetNumber
 * @property string $buildingFloor
 * @property string $buildingRoom
 * @property string $fullAddress
 * @property string $zip
 * @property string $town
 * @property string $department
 * @property string $country
 * @property string $phone
 * @property string $email
 * @property bool $disabled
 * @property float $lat
 * @property float $lng
 * @property int $receive
 *
 * @package Midla\TiendaenvioPhpSdk
 */
class TiendaEnvioAddress
{

    public $id;
    public $name;
    public $streetName;
    public $streetNumber;
    public $buildingFloor;
    public $buildingRoom;
    public $fullAddress;
    public $zip;
    public $town;
    public $department;
    public $country;
    public $phone;
    public $email;
    public $disabled;
    public $lat;
    public $lng;
    public $receive;


    /**
     * TiendaEnvioAddress constructor.
     * @param int|null $id
     * @param string|null $name
     * @param string|null $streetName
     * @param string|null $streetNumber
     * @param string|null $buildingFloor
     * @param string|null $buildingRoom
     * @param string|null $fullAddress
     * @param string|null $zip
     * @param string|null $town
     * @param string|null $department
     * @param string|null $country
     * @param string|null $phone
     * @param string|null $email
     * @param bool|null $disabled
     * @param string|null $lat
     * @param string|null $lng
     * @param int|null $receive
     */
    public function __construct(?int $id, ?string $name, ?string $streetName, ?string $streetNumber, ?string $buildingFloor, ?string $buildingRoom, ?string $fullAddress, ?string $zip, ?string $town, ?string $department, ?string $country, ?string $phone, ?string $email, ?bool $disabled, ?string $lat, ?string $lng, ?int $receive)
    {
        $this->id = $id;
        $this->name = $name;
        $this->streetName = $streetName;
        $this->streetNumber = $streetNumber;
        $this->buildingFloor = $buildingFloor;
        $this->buildingRoom = $buildingRoom;
        $this->fullAddress = $fullAddress;
        $this->zip = $zip;
        $this->town = $town;
        $this->department = $department;
        $this->country = $country;
        $this->phone = $phone;
        $this->email = $email;
        $this->disabled = $disabled;
        $this->lat = $lat;
        $this->lng = $lng;
        $this->receive = $receive;
    }



    public function getAddress()
    {
        $text  = $this->streetName . ' ';
        $text .= $this->streetNumber;

        if($this->buildingFloor || $this->buildingRoom){
            $text .= ' (';
            if($this->buildingFloor) $text .= $this->buildingFloor;
            if($this->buildingRoom)  $text .=  ' ' . $this->buildingRoom;
            $text .= ')';
        }
        return $text;
    }

    public function getAddressWithTown()
    {
        $text = $this->getAddress() . ', ' . $this->town;
        return $text;
    }

    public function getAddressWithZip()
    {
        $text = $this->getAddress() . ', ' . $this->zip;
        return $text;
    }
}