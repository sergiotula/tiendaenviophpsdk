<?php

namespace Midla\TiendaenvioPhpSdk;

class TiendaEnvioPackage{
    /** @var TiendaEnvioPackageProduct[] $package_products */
    private $package_products;


    private $weight;
    private $weightUnit;
    private $sizeHeight;
    private $sizeWidth;
    private $sizeDepth;
    private $declaredValue;
    private $quantity;

    private $description_1;





    public function __construct()
    {
        $this->package_products = [];
    }

    public function getFullWeight()
    {
        $full_weight = 0;
        foreach ($this->package_products as $product) {
            $full_weight += $product->getFullWeight();
        }
        return $full_weight;
    }

    public function getFullVolume()
    {
        $full_volume = 0;
        foreach ($this->package_products as $product) {
            $full_volume += $product->getFullVolume();
        }
        return $full_volume;
    }

    /**
     * @param TiendaEnvioProduct $product
     */
    public function addProduct($product)
    {
        foreach ($this->package_products as $package_product) {
            //If the product is already in the list, raise its quantity
            if($package_product->is($product)){
                $package_product->quantity++;
                $this->updateValues();
                return;
            }
        }
        //If there is no product yet in the package, create it
        $this->package_products[] = new TiendaEnvioPackageProduct($product);
        $this->updateValues();
    }
    public function updateValues(){
        $this->weight = 0;
        $this->weightUnit = 'KG';
        $this->sizeHeight = 0.001;
        $this->sizeWidth = 0.001;
        $this->sizeDepth = 0.001;
        $this->declaredValue = 0;
        $this->quantity = 1;
        foreach ($this->package_products as $package_product) {
            $this->weight += $package_product->getFullWeight();
        }
    }

    /**
     * @param TiendaEnvioProduct $product
     * @throws LimitException
     */
    public function forceAddProduct(TiendaEnvioProduct $product)
    {
        if($this->canFitProduct($product, 'with_exceptions')){

            foreach ($this->package_products as $package_product) {
                //If the product is already in the list, raise its quantity
                if($package_product->is($product)){
                    $package_product->quantity++;
                    $this->updateValues();
                    return;
                }
            }

            //If there is no product yet in the package, create it
            $this->package_products[] = $product;
            $this->updateValues();

        }
    }

    /**
     * @param TiendaEnvioProduct $product
     * @param string $mode
     * @return bool
     * @throws LimitException
     */
    public function canFitProduct(TiendaEnvioProduct $product, string $mode = 'passive')
    {
        return
            $this->hasValidDimensions($product) &&
            $this->hasEnoughSlots($mode) &&
            $this->hasEnoughWeight($product, $mode);
//            && $this->hasEnoughSpace($product);
    }

    /**
     * @param TiendaEnvioProduct $product
     * @return bool
     * @throws LimitException
     */
    public function hasValidDimensions(TiendaEnvioProduct $product)
    {
        $min = TiendaEnvioConst::DIMENSION_MIN_LIMIT_PER_PRODUCT;
        $max = TiendaEnvioConst::DIMENSION_MAX_LIMIT_PER_PRODUCT;

        $is_valid_width  = ($min <= $product->width ) && ($product->width <= $max);
        $is_valid_height = ($min <= $product->height ) && ($product->height <= $max);
        $is_valid_depth  = ($min <= $product->depth ) && ($product->depth <= $max);
        $is_valid_product = $is_valid_width && $is_valid_height && $is_valid_depth;
        if($is_valid_product){
            return true;
        }else{
            throw new LimitException("Product dimensions are invalid");
        }
    }

    /**
     * @param TiendaEnvioProduct $product
     * @param string $mode
     * @return bool
     * @throws LimitException
     */
    public function hasEnoughWeight(TiendaEnvioProduct $product, $mode = 'passive')
    {
        $predicted_weight = $product->weight + $this->getFullWeight();
        $is_valid_wight = $predicted_weight < TiendaEnvioConst::WEIGHT_LIMIT_PER_PACKAGE;

        switch($mode){
            case 'passive':
                return $is_valid_wight;
                break;
            case 'with_exceptions':
                if(!$is_valid_wight) throw new LimitException('Max unique products reached');
                return $is_valid_wight;
            default:
                return false;
                break;
        }

    }

    /**
     * @param string $mode
     * @return bool
     * @throws LimitException
     */
    public function hasEnoughSlots(string $mode = 'passive')
    {
        $unique_products_in_package = count($this->package_products);
        $has_enough_space = $unique_products_in_package < TiendaEnvioConst::PRODUCT_PER_PACKAGE_LIMIT;

        switch($mode){
            case 'passive':
                return $has_enough_space;
                break;
            case 'with_exceptions':
                if(!$has_enough_space) throw new LimitException('Max unique products reached');
                return $has_enough_space;
            default:
                return false;
            break;
        }
    }

    /**
     * @param TiendaEnvioProduct $product
     * @param string $mode
     * @return bool
     * @throws LimitException
     * @deprecated
     */
    public function hasEnoughVolume(TiendaEnvioProduct $product, $mode = 'passive')
    {
        $predicted_volume = $product->volume + $this->getFullVolume();
        $has_enough_volume =  $predicted_volume < TiendaEnvioConst::PACKAGE_VOLUME_LIMIT;

        switch($mode){
            case 'passive':
                return $has_enough_volume;
                break;
            case 'with_exceptions':
                if(!$has_enough_volume) throw new LimitException('Max unique products reached');
                return $has_enough_volume;
            default:
                return false;
                break;
        }

    }

    public function toArray()
    {
        $products_data = [];
        foreach ($this->package_products as $product) {
            $products_data[] = $product->toArray();
        }
        $package_data = [
            'weight' => $this->weight,
            'weightUnit' => $this->weightUnit,
            'sizeHeight' => $this->sizeHeight,
            'sizeWidth' => $this->sizeWidth,
            'sizeDepth' => $this->sizeDepth,
            'declaredValue' => $this->declaredValue,
            'quantity' => $this->quantity,

            'description_1' => $this->description_1,
            'products' => $products_data,
        ];
        return $package_data;
    }
}

