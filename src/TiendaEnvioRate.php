<?php

namespace Midla\TiendaenvioPhpSdk;

class TiendaEnvioRate{
    public $serviceId;
    public $price;
    public $priceTax;
    public $priceInsurance;
    public $priceTotal;
    public $name;
    public $code;
    public $currency;
    public $days;
    public $deliveryType;
    /** @var TiendaEnvioPickup[] $Pickups */
    public $Pickups;

    /**
     * TiendaEnvioRates constructor.
     * @param int $serviceId
     * @param float $price
     * @param float $priceTax
     * @param float $priceInsurance
     * @param float $priceTotal
     * @param string $name
     * @param string $code
     * @param string $currency
     * @param int $days
     * @param string $deliveryType
     * @param TiendaEnvioPickup[] $Pickups
     */
    public function __construct($serviceId, $price, $priceTax, $priceInsurance, $priceTotal, $name, $code, $currency, $days, $deliveryType, $Pickups)
    {
        $this->serviceId = $serviceId;
        $this->price = $price;
        $this->priceTax = $priceTax;
        $this->priceInsurance = $priceInsurance;
        $this->priceTotal = $priceTotal;
        $this->name = $name;
        $this->code = $code;
        $this->currency = $currency;
        $this->days = $days;
        $this->deliveryType = $deliveryType;
        $this->Pickups = $Pickups;
    }


}