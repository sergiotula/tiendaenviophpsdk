<?php

namespace Midla\TiendaenvioPhpSdk;

class TiendaEnvioConst{
    const WEIGHT_LIMIT_PER_PACKAGE = 25; //In KG
    const PRODUCT_PER_PACKAGE_LIMIT = 9;
    const PACKAGE_COUNT_LIMIT = 8;
    const DIMENSION_MIN_LIMIT_PER_PRODUCT = 0.001;//In CM
    const DIMENSION_MAX_LIMIT_PER_PRODUCT = 100;//In CM
    const PACKAGE_VOLUME_LIMIT = 100*100*100;//In CM3
}
