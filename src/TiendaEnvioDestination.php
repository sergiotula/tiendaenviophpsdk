<?php

namespace Midla\TiendaenvioPhpSdk;

/**
 * @property string $destName
 * @property null|string $destCode
 * @property null|string $destCodeType
 * @property null|string $destEmail
 * @property null|string $destStreetName
 * @property null|string $destStreetNumber
 * @property null|string $destBuildingFloor
 * @property null|string $destBuildingRoom
 * @property null|string $destZip
 * @property null|string $destTown
 * @property null|int $destDepartmentId
 * @property null|int $destCountryId
 * @property null|string $destPhone
 * @property null|string $hourFrom
 * @property null|string $hourTo
 */class TiendaEnvioDestination
{

    public $destName;
    public $destCode = null;
    public $destCodeType = null;
    public $destEmail = null;
    public $destStreetName = null;
    public $destStreetNumber = null;
    public $destBuildingFloor = null;
    public $destBuildingRoom = null;
    public $destZip = null;
    public $destTown = null;
    public $destDepartmentId = null;
    public $destCountryId = null;
    public $destPhone = null;
    public $hourFrom = null;
    public $hourTo = null;

    /**
     * TiendaEnvioLocation constructor.
     *
     * @param string $destName
     * @param null|string $destCode
     * @param null|string $destCodeType
     * @param null|string $destEmail
     * @param null|string $destStreetName
     * @param null|string $destStreetNumber
     * @param null|string $destBuildingFloor
     * @param null|string $destBuildingRoom
     * @param null|string $destZip
     * @param null|string $destTown
     * @param null|int $destDepartmentId
     * @param null|int $destCountryId
     * @param null|string $destPhone
     * @param null|string $hourFrom
     * @param null|string $hourTo
     */
    public function __construct($destName, $destCode, $destCodeType, $destEmail, $destStreetName, $destStreetNumber, $destBuildingFloor, $destBuildingRoom, $destZip, $destTown, $destDepartmentId, $destCountryId, $destPhone, $hourFrom, $hourTo)
    {
        $this->destName = $destName;
        $this->destCode = $destCode;
        $this->destCodeType = $destCodeType;
        $this->destEmail = $destEmail;
        $this->destStreetName = $destStreetName;
        $this->destStreetNumber = $destStreetNumber;
        $this->destBuildingFloor = $destBuildingFloor;
        $this->destBuildingRoom = $destBuildingRoom;
        $this->destZip = $destZip;
        $this->destTown = $destTown;
        $this->destDepartmentId = $destDepartmentId;
        $this->destCountryId = $destCountryId;
        $this->destPhone = $destPhone;
        $this->hourFrom = $hourFrom;
        $this->hourTo = $hourTo;
    }


    public function toArray()
    {
        return [
            'destName' => $this->destName,
            'destCode' => $this->destCode,
            'destCodeType' => $this->destCodeType,
            'destEmail' => $this->destEmail,
            'destStreetName' => $this->destStreetName,
            'destStreetNumber' => $this->destStreetNumber,
            'destBuildingFloor' => $this->destBuildingFloor,
            'destBuildingRoom' => $this->destBuildingRoom,
            'destZip' => $this->destZip,
            'destTown' => $this->destTown,
            'destDepartmentId' => $this->destDepartmentId,
            'destCountryId' => $this->destCountryId,
            'destPhone' => $this->destPhone,
            'hourFrom' => $this->hourFrom,
            'hourTo' => $this->hourTo,
        ];
    }




}