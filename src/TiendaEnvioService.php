<?php

namespace Midla\TiendaenvioPhpSdk;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;

class TiendaEnvioService implements TiendaEnvioInterface {

    /** @var string $api_key */
    private $api_key;
    /** @var string $api_token */
    private $api_token;
    /** @var TiendaEnvioPackage[] $packages */
    private $packages;
    /** @var int $current_package */
    private $current_package;
    /** @var TiendaEnvioRate[]|boolean $rates_data*/
    public $rates_data;
    /** @var string $error_information */
    public $error_information;

    /** @var string $base_url */
//    public $base_url = 'https://api-test.tiendaenvio.com.ar';//DEVELOPMENT PURPOSES ONLY
    public $base_url = 'https://api.tiendaenvio.com.ar';
    /** @var string $version */
    public $version = 'v1';
    /** @var string $url */
    public $url;

    /** @var bool $valid_keys */
    public $valid_keys = false;


    /**
     * TiendaEnvioPackages constructor.
     * @param string $api_key
     * @param string $api_token
     * @param bool $manual
     * @param int $packages_amount
     * @throws LimitException
     */
    public function __construct($api_key, $api_token, bool $manual = false, int $packages_amount = 1)
    {
        if($packages_amount <= 0) throw new LimitException('Minimum packages cannot be lower than zero');
        if($packages_amount > TiendaEnvioConst::PACKAGE_COUNT_LIMIT) throw new LimitException('Maximum packages cannot be greater than '. TiendaEnvioConst::PACKAGE_COUNT_LIMIT);

        $this->api_key = $api_key;
        $this->api_token = $api_token;

        if (!$manual) {
            $this->packages = [new TiendaEnvioPackage()];
            $this->current_package = 0;
        } else {
            $this->packages = [];
            for($i = 0; $i < $packages_amount; $i++){
                $this->packages[] = new TiendaEnvioPackage();
            }
        }

        $this->url = $this->base_url . '/' . $this->version;
    }

    /**
     * @param TiendaEnvioProduct $product
     * @throws LimitException
     */
    private function addProductToPackage(TiendaEnvioProduct $product)
    {
        //If the product can NOT enter in the current package, create a new package
        if (!$this->packages[$this->current_package]->canFitProduct($product)) {
            $packageCount = count($this->packages);
            if (($packageCount + 1) >= TiendaEnvioConst::PACKAGE_COUNT_LIMIT) {
                throw new LimitException(
                    'Cannot add product. Maximum amount of packages reached'
                );
            }
            $this->current_package++;
            $this->packages[$this->current_package] = new TiendaEnvioPackage();
        }

        $this->packages[$this->current_package]->addProduct($product);
    }

    /**
     * Adds a product to the package manager
     *
     * If the limit is reached on packages and the product cannot be added, it will throw a LimitException
     * If successful returns 'true'
     *
     * @param mixed $identifier
     * @param float $weight
     * @param string $weight_unit
     * @param float $width
     * @param float $height
     * @param float $depth
     * @param float $price
     * @param string $name
     * @param string $sku
     * @param string $code
     * @param string $description
     * @param int $quantity
     * @throws LimitException
     * @return boolean
     */
    public function addProduct($identifier, $weight, $weight_unit, $width, $height, $depth, $price, $name, $sku, $code, $description, $quantity = 1)
    {
        $product = new TiendaEnvioProduct($identifier, $weight, $weight_unit, $width, $height, $depth, $price, $name, $sku, $code, $description);
        for($i = 0; $i < $quantity; $i++) {
            $this->addProductToPackage($product);
        }
        return true;
    }

    /**
     * @param int $package_index
     * @param TiendaEnvioProduct $product
     * @param int $quantity
     * @return bool
     * @throws LimitException
     * @throws InvalidArgumentException
     */
    public function manualAddProduct(int $package_index, TiendaEnvioProduct $product, int $quantity = 1)
    {
        if (($package_index < 0) || $package_index >= ((count($this->packages) - 1))){
            throw new InvalidArgumentException('Package does not exist');
        }
        if(($quantity < 0)){
            throw new InvalidArgumentException('Quantity cannot be less than 1');
        }

        for($i = 0; $i < $quantity; $i++){
            $this->packages[$package_index]->forceAddProduct($product);
        }

        return true;
    }

    /**
     * Sends a request to get shipping rates
     *
     * Returns the shipping methods available or 'false' if the request failed
     * Failed request information is provided on $error_information
     *
     * @param $source_address_id int
     * @param $dest_zip string
     * @return bool|TiendaEnvioRate[]
     */
    public function requestRates($source_address_id, $dest_zip)
    {
        $client = new Client();

        $packages_data = [];
        foreach ($this->packages as $package) {
            $packages_data[] = $package->toArray();
        }
        $rateRequestData = [
            'addressId'=> $source_address_id,
            'destZip'=> $dest_zip,
            'Packages' => $packages_data,
        ];

        try {
            $response = $this->send_post('/delivery/price', $rateRequestData);
        } catch (GuzzleException $e) {
            return false;
        }

        if (!isset($response->Prices)) {
            return false;
        }

        $methods = [];
        foreach ($response->Prices as $method) {
            $methods[] = new TiendaEnvioRate(
                $method->serviceId,
                $method->price,
                $method->priceTax,
                $method->priceInsurance,
                $method->priceTotal,
                $method->name,
                $method->code,
                $method->currency,
                $method->days,
                $method->deliveryType,
                $method->Pickups
            );
        }
        $this->rates_data = $methods;
        return $methods;


    }

    /**
     * Sends a request to create a new shipment
     *
     * @param int $origin_id
     * @param string $origin_code
     * @param string $delivery_type
     * @param int $pick_up_point_id
     * @param int $address_id
     * @param string $service_code
     * @param string $comments
     * @param int $customer_tracking_id
     * @param TiendaEnvioDestination $destination
     * @return bool|mixed
     */
    public function requestCreateShipment(
        $origin_id, $origin_code, $delivery_type, $pick_up_point_id, $address_id, $service_code, $comments, $customer_tracking_id,
        $destination
    )
    {
        /** @var TiendaEnvioPackage[] $package_data */
        $package_data = [];
        foreach ($this->packages as $package) {
            $package_data[] = $package->toArray();
        }
        $request_data = [
            'shipment' => [
                'origenId'              => $origin_id,
                'origenCode'            => $origin_code,
                'deliveryType'          => $delivery_type,
                'pickUpPointId'         => $pick_up_point_id,
                'addressId'             => $address_id,
                'serviceCode'           => $service_code,
                'comments'              => $comments,
                'customerTrackingId'    => $customer_tracking_id,//Numero de orden del ecommerce TODO

                'packages'              => $package_data,

                'destination'           => $destination->toArray()
            ]
        ];


        try {
            $response = $this->send_post('/delivery', $request_data);
            return $response;

        } catch (GuzzleException $e) {
            $this->error_information = $e->getMessage();
            return false;

        }

    }

    /**
     * @return TiendaEnvioAddress[]
     */
    public function getAddresses()
    {
        $client = new Client();
        $url = $this->getUrl() . '/address';
        try {
            $response = $client->get($url, [
                'headers' => $this->getHeaders()
            ]);

            $response = json_decode($response->getBody()->getContents());

            $addresses = [];
            foreach ($response->Address as $address) {

                $addresses[] = new TiendaEnvioAddress(
                    isset($address->id) ? $address->id : '',
                    isset($address->name) ? $address->name : '',
                    isset($address->streetName) ? $address->streetName : '',
                    isset($address->streetNumber) ? $address->streetNumber : '',
                    isset($address->buildingFloor) ? $address->buildingFloor : '',
                    isset($address->buildingRoom) ? $address->buildingRoom : '',
                    isset($address->fullAddress) ? $address->fullAddress : '',
                    isset($address->zip) ? $address->zip : '',
                    isset($address->town) ? $address->town : '',
                    isset($address->department) ? $address->department : '',
                    isset($address->country) ? $address->country : '',
                    isset($address->phone) ? $address->phone : '',
                    isset($address->email) ? $address->email : '',
                    isset($address->disabled) ? $address->disabled : '',
                    isset($address->lat) ? $address->lat : '',
                    isset($address->lng) ? $address->lng : '',
                    isset($address->receive) ? $address->receive : ''
                );
            }

            return $addresses;

        } catch (GuzzleException $e) {
            return null;
        } catch (Exception $e) {
            return null;
        }

    }

    /**
     * @param $name string
     * @return TiendaEnvioCountry[]|null
     */
    public function findCountry($name)
    {
        try {
            $query = '?name='.$name;
            $response = $this->send_get('/localization/country' . $query);
            $countries = $response->data;
            $result = [];
            foreach ($countries as $country) {
                $result[] = new TiendaEnvioCountry(
                    $country->id,
                    $country->code,
                    $country->code_iso,
                    $country->name,
                    $country->code_maria
                );
            }
            return $result;
        } catch (GuzzleException $e) {
            return null;
        }
    }

    /**
     * @return TiendaEnvioCountry[]|null
     */
    public function getCountries()
    {
        try {
            return $this->send_get('/localization/country');
        } catch (GuzzleException $e) {
            return null;
        }
    }

    /**
     * @param $country
     * @return TiendaEnvioProvince[]|null
     */
    public function getProvinces($country)
    {
        try {
            $query = '?countryId=' . $country;
            $provinces = $this->send_get('/localization/department' . $query);
            $result = [];
            foreach ($provinces[0] as $province) {
                $result[] = new TiendaEnvioProvince($province->id, $province->name);
            }
            return $result;
        } catch (GuzzleException $e) {
            return null;
        }
    }




    public function getTracking($envio_id)
    {
        // TODO: Implement get_tracking() method.
        return '0';
    }

    public function getTrackingStatus($tracking)
    {
        // TODO: Implement get_tracking_status()
        return 'Method not yet implemented';
    }


    /**
     * Validates the keys that were input in the constructor
     *
     * Returns 'true' if the keys are valid. Returns 'false' on any other case
     *
     * @return bool
     */
    public function validateKeys()
    {

        try {
            $response = $this->send_get('/keys');
            $this->valid_keys = true;
        } catch (GuzzleException $e) {
            $this->valid_keys = false;
        }
        return $this->valid_keys;

    }





    private function getHeaders()
    {
        return [
            'X-Midla-App-key' => $this->api_key,
            'X-Midla-App-Token' => $this->api_token,
            'Content-Type' => 'application/json',
        ];
    }

    private function getUrl()
    {
        $this->url = $this->base_url . '/' . $this->version;
        return $this->url;
    }


    /**
     * @param $endpoint
     * @param $data
     * @return mixed
     * @throws GuzzleException
     */
    private function send_post($endpoint, $data)
    {
        $client = new Client();

        $url = $this->getUrl() . $endpoint;


        $response = $client->post($url, [
            'headers' => $this->getHeaders(),
            'json' => $data,
        ]);

        return json_decode($response->getBody()->getContents());

    }

    /**
     * @param $endpoint
     * @return mixed
     * @throws GuzzleException
     */
    private function send_get($endpoint)
    {
        $client = new Client();

        $url = $this->getUrl() . $endpoint;


        $response = $client->get($url, [
            'headers' => $this->getHeaders(),
        ]);

        return json_decode($response->getBody()->getContents());

    }
}





