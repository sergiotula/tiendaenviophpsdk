<?php


namespace Midla\TiendaenvioPhpSdk;

/**
 * Class TiendaEnvioProvince
 * @package Midla\TiendaenvioPhpSdk
 * @property $id int
 * @property $name string
 */
class TiendaEnvioProvince
{
    public $id;
    public $name;

    /**
     * TiendaEnvioProvince constructor.
     * @param $id int
     * @param $name string
     */
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }


}