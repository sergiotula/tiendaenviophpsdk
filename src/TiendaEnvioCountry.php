<?php

namespace Midla\TiendaenvioPhpSdk;

/**
 * Class TiendaEnvioCountry
 * @package Midla\TiendaenvioPhpSdk
 * @property $id int
 * @property $code string
 * @property $code_iso string
 * @property $name string
 * @property $code_maria int
 */
class TiendaEnvioCountry{
    public $id;
    public $code;
    public $code_iso;
    public $name;
    public $code_maria;

    /**
     * TiendaEnvioCountry constructor.
     * @param $id
     * @param $code
     * @param $code_iso
     * @param $name
     * @param $code_maria
     */
    public function __construct($id, $code, $code_iso, $name, $code_maria)
    {
        $this->id = $id;
        $this->code = $code;
        $this->code_iso = $code_iso;
        $this->name = $name;
        $this->code_maria = $code_maria;
    }


}
