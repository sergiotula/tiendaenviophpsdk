<?php

namespace Midla\TiendaenvioPhpSdk;

use InvalidArgumentException;

interface TiendaEnvioInterface{

    /**
     * Adds a product to the package manager
     *
     * If the limit is reached on packages and the product cannot be added, it will throw a LimitException
     * If successful returns 'true'
     *
     * @param mixed $identifier
     * @param float $weight
     * @param string $weight_unit
     * @param float $width
     * @param float $height
     * @param float $depth
     * @param float $price
     * @param string $name
     * @param string $sku
     * @param string $code
     * @param string $description
     * @param int $quantity
     * @throws LimitException
     * @return boolean
     */
    public function addProduct($identifier, $weight, $weight_unit, $width, $height, $depth, $price, $name, $sku, $code, $description, $quantity = 1);

    /**
     * Sends a request to get shipping rates
     *
     * Returns the shipping methods available or 'false' if the request failed
     * Failed request information is provided on $error_information
     *
     * @param $source_address_id int
     * @param $dest_zip string
     * @return bool|TiendaEnvioRate[]
     */
    public function requestRates($source_address_id, $dest_zip);

    /**
     * Sends a request to create a new shipment
     *
     * @param int $origin_id
     * @param string $origin_code
     * @param string $delivery_type
     * @param int $pick_up_point_id
     * @param int $address_id
     * @param string $service_code
     * @param string $comments
     * @param int $customer_tracking_id
     * @param TiendaEnvioDestination $destination
     * @return bool|mixed
     */
    public function requestCreateShipment($origin_id, $origin_code, $delivery_type, $pick_up_point_id, $address_id, $service_code, $comments, $customer_tracking_id,
                                          $destination);


    /**
     * @param int $package_index
     * @param TiendaEnvioProduct $product
     * @param int $quantity
     * @return bool
     * @throws LimitException
     * @throws InvalidArgumentException
     */
    public function manualAddProduct(int $package_index, TiendaEnvioProduct $product, int $quantity = 1);
    
    
    
    /**
     * @return TiendaEnvioAddress[]
     */
    public function getAddresses();


    /**
     * @param $name string
     * @return TiendaEnvioCountry[]|null
     */
    public function findCountry($name);

    /**
     * @return TiendaEnvioCountry[]|null
     */
    public function getCountries();

    /**
     * @param $country
     * @return TiendaEnvioProvince[]|null
     */
    public function getProvinces($country);








    /**
     * Gets the shipment tracking number
     *
     * @param $envio_id int
     * @return mixed string
     */
    public function getTracking($envio_id);

    /**
     * Gets the shipment current status
     *
     *
     * @param $tracking int The tracking number
     * @return mixed
     */
    public function getTrackingStatus($tracking);

    /**
     * Validates the keys that were input in the constructor
     *
     * Returns 'true' if the keys are valid. Returns 'false' on any other case
     *
     * @return bool
     */
    public function validateKeys();
}