<?php

namespace Midla\TiendaenvioPhpSdk;



class TiendaEnvioPackageProduct{
    /** @var int $quantity */
    public $quantity;
    /** @var TiendaEnvioProduct $product */
    public $product;

    /**
     * TiendaEnvioPackageProduct constructor.
     * @param TiendaEnvioProduct $product
     */
    public function __construct(TiendaEnvioProduct $product)
    {
        $this->product = $product;
        $this->quantity = 1;
    }


    public function getFullWeight()
    {


        return $this->quantity * $this->product->weight;
    }

    public function getFullVolume()
    {
        return $this->quantity * $this->product->volume;
    }

    /**
     * @param TiendaEnvioProduct $product
     * @return bool
     */
    public function is($product)
    {
        return $product->getIdentifier() == $this->product->getIdentifier();
    }

    public function toArray()
    {
        return [
            'name'          => $this->product->name,
            'weight'        => $this->product->weight,
            'weightUnit'    => $this->product->weight_unit,
            'sizeHeight'    => $this->product->height,
            'sizeWidth'     => $this->product->width,
            'sizeDepth'     => $this->product->depth,
            'code'          => $this->product->code,
            'count'         => $this->quantity,
            'price'         => $this->product->price,
            'sku'           => $this->product->sku,
            'description'   => $this->product->description,

        ];
    }
}

