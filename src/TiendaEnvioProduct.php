<?php

namespace Midla\TiendaenvioPhpSdk;


class TiendaEnvioProduct{
    /** @var mixed identifier */
    private $identifier;
    /** @var float $weight */
    public $weight;
    /** @var string $weight_unit */
    public $weight_unit;
    /** @var float $width */
    public $width;
    /** @var float $height */
    public $height;
    /** @var float $depth */
    public $depth;
    /** @var float $price */
    public $price;
    /** @var string $name */
    public $name;
    /** @var string $sku */
    public $sku;
    /** @var string $code */
    public $code;
    /** @var string $description */
    public $description;


    /** @var float $volume */
    public $volume;


    /**
     * TiendaEnvioProduct constructor.
     * @param mixed $identifier
     * @param float $weight
     * @param string $weight_unit
     * @param float $width
     * @param float $height
     * @param float $depth
     * @param float $price
     * @param string $name
     * @param string $sku
     * @param string $code
     * @param string $description
     */
    public function __construct($identifier, $weight, $weight_unit, $width, $height, $depth, $price, $name, $sku, $code, $description)
    {
        $this->identifier = $identifier;

        $this->weight = $weight;
        $this->weight_unit = $weight_unit;
        $this->width = $width;
        $this->height = $height;
        $this->depth = $depth;
        $this->price = $price;
        $this->name = $name;
        $this->sku = $sku;
        $this->code = $code;
        $this->description = $description;

        $this->volume = $width*$height*$depth;
    }


    public function getIdentifier()
    {
        return $this->identifier;
    }
}
