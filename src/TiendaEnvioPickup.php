<?php

namespace Midla\TiendaenvioPhpSdk;


class TiendaEnvioPickup{
    public $id;
    public $nom;
    public $streetName;
    public $streetNumber;
    public $buildingFloor;
    public $buildingRoom;
    public $zip;
    public $town;
    public $departmentName;
    public $countryName;
    public $phone;
    public $email;
    public $latitud;
    public $longitud;
    public $geolocalizacion;
    /** @var TiendaEnvioOpenDateInfo $opendate_info */
    public $opendate_info;
}